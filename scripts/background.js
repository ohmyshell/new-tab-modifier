"use strict";

chrome.runtime.onInstalled.addListener(function () {
    chrome.alarms.create('fetch', { periodInMinutes: 60 * 6 })
    chrome.alarms.create('checkIntegrity', { periodInMinutes: 5 })
    chrome.alarms.create('updateWeather', { periodInMinutes: 20 })
    fetchImage()
    fetchQuote()
    fetchXKCD()
    fetchWeather()
})

chrome.alarms.onAlarm.addListener(function (alarm) {
    if (alarm && alarm.name == 'updateWeather') {
        let weather = fetchWeather()
        console.info("Weather fetched: " + weather)
    }
    if (alarm && alarm.name == 'fetch') {
        let img = fetchImage()
        let quote = fetchQuote()
        let xkcd = fetchXKCD()

        console.info("Image fetched: " + img)
        console.info("Quote fetched: " + quote)
        console.info("XKCD fetched: " + xkcd)
    }
    if (alarm && alarm.name == 'checkIntegrity') {
        let img = JSON.parse(localStorage.getItem("image"))
        let quote = JSON.parse(localStorage.getItem("quote"))
        let xkcd = JSON.parse(localStorage.getItem("comic"))

        console.info("Integrity Checked.")

        if (!img) {
            let img = fetchImage()
            console.info("Image fetched: " + img)
        }
        if (!quote) {
            let quote = fetchQuote()
            console.info("Quote fetched: " + quote)
        }
        if (!xkcd) {
            let xkcd = fetchXKCD()
            console.info("XKCD fetched: " + xkcd)
        }
    }
})

//get weather
function fetchWeather() {
    let getCoordinates = function () {
        return new Promise(function (resolve, reject) {
            navigator.geolocation.getCurrentPosition(resolve, reject);
        });
    }
    getCoordinates()
        .then(function(location){
            fetch(`https://mycorsanywhere.herokuapp.com/https://api.openweathermap.org/data/2.5/weather?lat=${location.coords.latitude.toFixed(0)}&lon=${location.coords.longitude.toFixed(0)}&APPID=8b7e9027cd14d6fc66a3fa1bf175f4fe`)
                .then(res=>res.json())
                .then(json=>localStorage.setItem('weather', JSON.stringify(json)))
        })
    return true;
}

//get Image
function fetchImage() {
    fetch(`https://source.unsplash.com/random/${window.screen.availWidth}x${window.screen.availHeight}`)
        .then(res => {
            return res.blob()
        })
        .then(blob => {
            var reader = new FileReader();
            reader.readAsDataURL(blob);
            reader.onloadend = function () {
                let base64data = reader.result;
                localStorage.setItem("image", JSON.stringify({
                    "data": base64data,
                    "time": Date.now()
                }))
            }
        })
    return true;
}

//get Quote
function fetchQuote() {
    fetch('http://quotes.rest/qod.json')
        .then(res => { return res.json() })
        .then(json => {
            localStorage.setItem("quote", JSON.stringify({
                "quote": json.contents.quotes[0].quote,
                "author": json.contents.quotes[0].author,
                "time": Date.now()
            }))
        })
    return true;
}

//get XKCD
function fetchXKCD() {
    fetch('https://mycorsanywhere.herokuapp.com/https://xkcd.com/info.0.json')
        .then(res => res.json())
        .then(json => {
            fetch('https://mycorsanywhere.herokuapp.com/' + json.img)
                .then(res => res.blob())
                .then(blob => {
                    let reader = new FileReader()
                    reader.readAsDataURL(blob)
                    reader.onloadend = function () {
                        let base64data = reader.result
                        localStorage.setItem("comic", JSON.stringify({
                            "data": base64data,
                            "alt": json.alt,
                            "time": Date.now()
                        }))
                    }
                })
        })
    return true;
}