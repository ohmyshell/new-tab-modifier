"use strict";

window.onload = function () {

    let img = JSON.parse(localStorage.getItem("image"));
    let quote = JSON.parse(localStorage.getItem("quote"));
    let xkcd = JSON.parse(localStorage.getItem("comic"));
    let weather = JSON.parse(localStorage.getItem("weather"));

    //set quote, image, and xkcd
    document.querySelector("body").style.backgroundImage = `url(${img.data})`;
    document.querySelector(".qod").innerHTML = `"${quote.quote}" -${quote.author}`;
    document.querySelector(".topcenter>img").src = xkcd.data;
    document.querySelector(".topcenter>img").title = xkcd.alt;


    let sunrise = new Date(weather.sys.sunrise).toLocaleTimeString("en-us", { hour: "2-digit", minute: "2-digit" })
    let sunset = new Date(weather.sys.sunset).toLocaleTimeString("en-us", { hour: "2-digit", minute: "2-digit" })
    //set temprature
    document.querySelector('.grid>.leftcenter>.location').innerHTML = `<i class="fas fa-map-marker"></i> ${weather.name}`
    document.querySelector('.grid>.leftcenter>.location').title = "Location"
    document.querySelector('.grid>.leftcenter>.templow').innerHTML = `<i class="fas fa-thermometer-empty"></i> ${(weather.main.temp_min - 273.15).toFixed(2)}`
    document.querySelector('.grid>.leftcenter>.templow').title = "Low"
    document.querySelector('.grid>.leftcenter>.temp').innerHTML = `<i class="fas fa-thermometer-half"></i> ${(weather.main.temp - 273.15).toFixed(2)}`
    document.querySelector('.grid>.leftcenter>.temp').title = "Temperature"
    document.querySelector('.grid>.leftcenter>.temphigh').innerHTML = `<i class="fas fa-thermometer-full"></i> ${(weather.main.temp_max - 273.15).toFixed(2)}`
    document.querySelector('.grid>.leftcenter>.temphigh').title = "High"
    document.querySelector('.grid>.leftcenter>.pressure').innerHTML = `<i class="fas fa-angle-double-down"></i> ${weather.main.pressure}`
    document.querySelector('.grid>.leftcenter>.pressure').title = "Pressure"
    document.querySelector('.grid>.leftcenter>.humidity').innerHTML = `<i class="fas fa-water"></i> ${weather.main.humidity}`
    document.querySelector('.grid>.leftcenter>.humidity').title = "Humidity"
    document.querySelector('.grid>.leftcenter>.windspeed').innerHTML = `<i class="fas fa-wind"></i> ${weather.wind.speed}`
    document.querySelector('.grid>.leftcenter>.windspeed').title = "Wind Speed"
    document.querySelector('.grid>.leftcenter>.winddirection').innerHTML = ` <i class="fas fa-location-arrow"></i> ${weather.wind.deg}`
    document.querySelector('.grid>.leftcenter>.winddirection').title = "Wind Direction"

    //set Time
    document.querySelector(".time").innerHTML = new Date().toLocaleTimeString("en-us", { hour: "2-digit", minute: "2-digit" });
    setInterval(function () {
        document.querySelector(".time").innerHTML = new Date().toLocaleTimeString("en-us", { hour: "2-digit", minute: "2-digit" });
    }, 1000)

    //set button event handler
    document.querySelector(".topleft>button").onclick = shortenURL;
    document.querySelector(".grid>.topright>button").onclick = showOverlay;
    document.querySelector(".overlay>.topright>button").onclick = hideOverlay;
}

function shortenURL(e) {
    e.preventDefault();
    fetch('https://mycorsanywhere.herokuapp.com/https://cleanuri.com/api/v1/shorten', {
        method: 'POST',
        mode: 'cors',
        headers: {
            'Content-Type': 'application/x-www-form-urlencoded',
        },
        body: `url=${document.querySelector("input").value}`
    })
        .then(res => res.json())
        .then(json => {
            document.querySelector(".shortened").href = json.result_url
            document.querySelector(".shortened").innerHTML = json.result_url
        })
}

function showOverlay(e) {
    e.preventDefault();
    document.querySelector('.overlay').classList = 'overlay show';
}

function hideOverlay(e) {
    e.preventDefault();
    document.querySelector('.overlay').classList = 'overlay hide';
}